# OpenML dataset: Covid-19-Case-Surveillance-Public-Use-Dataset

https://www.openml.org/d/43365

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context and Content
The COVID-19 case surveillance system database includes individual-level data reported to U.S. states and autonomous reporting entities, including New York City and the District of Columbia (D.C.), as well as U.S. territories and states. On April 5, 2020, COVID-19 was added to the Nationally Notifiable Condition List and classified as immediately notifiable, urgent (within 24 hours) by a Council of State and Territorial Epidemiologists (CSTE) Interim Position Statement (Interim-20-ID-01). CSTE updated the position statement on August 5, 2020 to clarify the interpretation of antigen detection tests and serologic test results within the case classification. The statement also recommended that all states and territories enact laws to make COVID-19 reportable in their jurisdiction, and that jurisdictions conducting surveillance should submit case notifications to CDC. COVID-19 case surveillance data are collected by jurisdictions and shared voluntarily with CDC.
For more information:
https://data.cdc.gov/Case-Surveillance/COVID-19-Case-Surveillance-Public-Use-Data/vbim-akqf
The deidentified data in the public use dataset include demographic characteristics, exposure history, disease severity indicators and outcomes, clinical data, laboratory diagnostic test results, and comorbidities. All data elements can be found on the COVID-19 case report form located at www.cdc.gov/coronavirus/2019-ncov/downloads/pui-form.pdf.

Acknowledgement
https://www.cdc.gov/

Inspiration

Covid-19 researches  e.g. Demographic Trends of COVID-19 cases and deaths

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43365) of an [OpenML dataset](https://www.openml.org/d/43365). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43365/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43365/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43365/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

